module Multi_structure exposing (..)

import Html exposing (..)
import Time exposing (..)
import Element exposing (..)
import Color exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Svg exposing (Svg, svg, circle, line, polygon, rect, g, text_, text)
import Svg.Attributes exposing (..)
import TreeDiagram exposing (..)
import TreeDiagram.Svg exposing (..)
--------------------------------------------------------------------------------

main : Program Never Model Msg
main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type alias Model =
  {   curnum  : Int
    , input   : List Int
    , treerb  : RBTree Int
    , showingRB : Bool
    --, more showing
    , speed   : Float
}

type Msg = Append
  | Prepend
  | Change String
  | Increment_Speed
  | Decrement_Speed
  | Insert_Element Time
  | ToggleRBTree
  --| MoreTreeToggles


init : (Model, Cmd Msg)
init = (initialModel, Cmd.none)

initialModel : Model
initialModel =
  {
      curnum = 0
    , input = []
    , treerb = RBE
    , showingRB = False
    , speed = 10.0
   }

subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every (model.speed * Time.second) Insert_Element


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  (
    case msg of
      Prepend ->
        { model | input = model.curnum :: model.input  }

      Append ->
        { model | input = model.input ++ [model.curnum]  }

      Change text ->
        { model | curnum = Result.withDefault 0 (String.toInt text) }

      Increment_Speed ->
        if model.speed == 0.0 then
          { model | speed = (model.speed + 0.5) }
        else if model.speed == 0.5 then
          { model | speed = (model.speed + 0.5) }
        else
          { model | speed = (model.speed + 1) }

      Decrement_Speed ->
        if model.speed == 1.0 then
          { model | speed = (model.speed - 0.5) }
        else if model.speed == 0.5 then
          { model | speed = (model.speed - 0.5) }
        else if model.speed == 0.0 then
          { model | speed = (model.speed) }
        else
          { model | speed = (model.speed - 1) }

      Insert_Element t ->
        if model.input == [] then
            { model | input = [] }
        else case model.input of
            head :: tail ->
              if model.showingRB == True then
                let treerb_ = rbinsert head model.treerb in
                { model | treerb = treerb_, input = tail }
              else
                { model | input = tail}

            _ -> Debug.crash "??"

      ToggleRBTree ->
        { model | showingRB = not model.showingRB }

          , Cmd.none )

view : Model -> Html Msg
view model =
  -- Debug.crash "??"
    div [] [
      if model.showingRB == True then
           draw
            { defaultTreeLayout | padding = 60, siblingDistance = 80 }
            drawNode
            drawEdge
            (rbmapToTree model.treerb)
      else Html.text "Should a tree be selected?"
      ,  div [] [ input [ Html.Attributes.type_ "text", placeholder "Insert into the list", onInput Change ] []
      , button [ onClick Prepend          ] [ Html.text "prepend" ]
      , button [ onClick Append           ] [ Html.text "append" ]
      , div [] [ Html.text (toString model.input) ]
      , button [ onClick (Insert_Element model.speed )] [Html.text "Insert into Tree "]
      , button [ onClick (Decrement_Speed) ] [ Html.text "-" ]
      , button [ onClick (Increment_Speed)    ] [ Html.text "+" ]
      , div [] [ Html.text ("Will insert new node every ")
      , Html.text (toString model.speed)
      , Html.text (" seconds.") ]
      , checkbox ToggleRBTree "Use Red Black Tree?"
      ] ]

checkbox : msg -> String -> Html msg
checkbox msg name =
  label
    [ Html.Attributes.style [("padding", "20px")]
    ]
    [ input [ Html.Attributes.type_ "checkbox", onClick msg ] []
    , Html.text name
    ]

--  if model.showingRB == True then
  --  let (_, color, _, n, _) = model.treerb in

--------------------------------------------------------------------------------

type Color  = BlackBlack   -- +2  "douBlacklackle black" ("double black")
            | Black    -- +1
            | Red    --  0
            | RedRed   -- -1  "double red"   ("negative black")

type RBTree a = RBE    --  0  "empty"        ("black leaf"        +1)
            | BlackE   -- +1  "black empty"  ("double black leaf" +2)
            | T Color (RBTree a) a (RBTree a)

type alias RBBalance comparable =
  Color -> RBTree comparable -> comparable -> RBTree comparable -> RBTree comparable

type alias RBBalanceMaybe comparable =
  Color -> RBTree comparable -> comparable -> RBTree comparable -> Maybe (RBTree comparable)

rbincr c =
  case c of
    BlackBlack -> Debug.crash "incr BB"
    Black  -> BlackBlack
    Red  -> Black
    RedRed -> Red

rbdecr c =
  case c of
    BlackBlack -> Black
    Black  -> Red
    Red  -> RedRed
    RedRed -> Debug.crash "decr RR"

rbempty = RBE

rbmember : comparable -> RBTree comparable -> Bool
rbmember x t = case t of
  RBE -> False
  T _ l y r ->
    if x == y then True
    else if x < y then rbmember x l
    else rbmember x r
  BlackE -> Debug.crash "member BE"

rbinsert : comparable -> RBTree comparable -> RBTree comparable
rbinsert x t =
  case rbins x t of
    T _ l y r -> T Black l y r
    RBE         -> Debug.crash "rbinsert"
    _         -> Debug.crash "insert E"

rbins : comparable -> RBTree comparable -> RBTree comparable
rbins x t =
  case t of
    RBE -> T Red RBE x RBE
    T c l y r ->
      if x == y then t
      else if x < y then rbbalance c (rbins x l) y r
      else rbbalance c l y (rbins x r)
    BlackE -> Debug.crash "ins BE"

rbbalance : RBBalance comparable
rbbalance c l val r =
  rbbalance_B_R_R c l val r
  |> rbmaybePlus (rbbalance_BB_R_R c l val r)
  |> rbmaybePlus (rbbalance_BB_RR c l val r)
  |> Maybe.withDefault (T c l val r)

rbbalance_B_R_R : RBBalanceMaybe comparable
rbbalance_B_R_R color l val r =
  case (color, l, val, r) of
    (Black, T Red (T Red a x b) y c, z, d) -> Just <| T Red (T Black a x b) y (T Black c z d)
    (Black, T Red a x (T Red b y c), z, d) -> Just <| T Red (T Black a x b) y (T Black c z d)
    (Black, a, x, T Red (T Red b y c) z d) -> Just <| T Red (T Black a x b) y (T Black c z d)
    (Black, a, x, T Red b y (T Red c z d)) -> Just <| T Red (T Black a x b) y (T Black c z d)
    _                              -> Nothing

rbbalance_BB_R_R : RBBalanceMaybe comparable
rbbalance_BB_R_R color l val r =
  case (color, l, val, r) of
    (BlackBlack, T Red (T Red a x b) y c, z, d) -> Just <| T Black (T Black a x b) y (T Black c z d)
    (BlackBlack, T Red a x (T Red b y c), z, d) -> Just <| T Black (T Black a x b) y (T Black c z d)
    (BlackBlack, a, x, T Red (T Red b y c) z d) -> Just <| T Black (T Black a x b) y (T Black c z d)
    (BlackBlack, a, x, T Red b y (T Red c z d)) -> Just <| T Black (T Black a x b) y (T Black c z d)
    _                               -> Nothing

rbbalance_BB_RR : RBBalanceMaybe comparable
rbbalance_BB_RR color l val r =
  case (color, l, val, r) of
    (BlackBlack, T RedRed (T Black a w b) x (T Black c y d), z, e) -> Just <| T Black (rbbalance Black (T Red a w b) x c) y (T Black d z e)
    (BlackBlack, a, w, T RedRed (T Black b x c) y (T Black d z e)) -> Just <| T Black (T Black a w b) x (rbbalance Black c y (T Red d z e))
    _                                          -> Nothing

rbbubble_BE_and_BB : RBTree comparable -> RBTree comparable
rbbubble_BE_and_BB t =
  case t of

    -- cases 1a, 2a, 3a
    T c1 (T c2 a x b) y BlackE ->
      case (c1, c2) of
        (Red, Black) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y RBE
        (Black, Black) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y RBE
        (Black, Red) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y RBE
        _      -> t

    -- cases 1b, 2b, 3b
    T c1 BlackE y (T c3 c z d) ->
      case (c1, c3) of
        (Red, Black) -> rbbalance (rbincr c1) RBE y (T (rbdecr c3) c z d)
        (Black, Black) -> rbbalance (rbincr c1) RBE y (T (rbdecr c3) c z d)
        (Black, Red) -> rbbalance (rbincr c1) RBE y (T (rbdecr c3) c z d)
        _      -> t

    -- cases 1a', 1b', 2a', 2b', 3a', 3b'
    T c1 (T c2 a x b) y (T c3 c z d) ->
      case (c1, c2, c3) of
        (Red, Black, BlackBlack) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        (Red, BlackBlack, Black) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        (Black, Black, BlackBlack) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        (Black, BlackBlack, Black) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        (Black, Red, BlackBlack) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        (Black, BlackBlack, Red) -> rbbalance (rbincr c1) (T (rbdecr c2) a x b) y (T (rbdecr c3) c z d)
        _          -> t

    _ -> t
------------------------------------------------------------------------------
rbremove : comparable -> RBTree comparable -> RBTree comparable
rbremove x t =
  case rbrem x t of
    T _ l y r -> T Black l y r
    _         -> Debug.crash "remove E"

rbrem : comparable -> RBTree comparable -> RBTree comparable
rbrem n t =
  case t of

    BlackE -> Debug.crash "rem BE"
    RBE  -> RBE

    -- 0 children
    T Red RBE x RBE -> if n == x then BlackE else t
    T Black RBE x RBE-> if n == x then T BlackBlack RBE x RBE else t

    -- 1 child
    T Black (T Red RBE x RBE) y RBE-> if n == y then T Black RBE x RBE else t
    T Black RBE y (T Red RBE z RBE) -> if n == y then T Black RBE z RBE else t
    T _ RBE _ _           -> Debug.crash "rem"
    T _ _ _  RBE          -> Debug.crash "rem"

    -- 2 children
    T c l y r ->
      if n < y then rbbalance c (rbbubble_BE_and_BB (rbrem n l)) y r
      else if n > y then rbbalance c l y (rbbubble_BE_and_BB (rbrem n r))
      else {- n == y -} rbrem_2_children c l y r

rbrem_2_children : Color -> RBTree comparable -> comparable -> RBTree comparable -> RBTree comparable
rbrem_2_children c left y right =
  let remove_max t =
    case t of
      T Red RBE x RBE           -> (x, RBE)          -- cases 1a/1b
      T Black RBE x RBE           -> (x, BlackE)         -- cases 2a/2b
      T Black (T Red RBE w RBE) x RBE -> (x, T Black RBE w RBE)  -- cases 3a/3b

      T c l v r ->
        let (x, r2) = remove_max r in
        (x, T c l v r2)

      _ -> Debug.crash "rem_2_children"
  in
  let (x, new_left) = remove_max left in
  T c new_left x right

rbmaybePlus : Maybe a -> Maybe a -> Maybe a
rbmaybePlus mx my =
  case mx of
    Just x  -> mx
    Nothing -> my

rbcolor t = case t of
  T c _ _ _ -> c
  RBE         -> Black
  BlackE        -> Black

rbroot t = case t of
  T _ _ x _ -> x
  RBE         -> Debug.crash "root"
  BlackE        -> Debug.crash "root"
rbleft t = case t of
  T _ l _ _ -> l
  RBE         -> Debug.crash "left"
  BlackE        -> Debug.crash "left"
rbright t = case t of
  T _ _ _ r -> r
  RBE         -> Debug.crash "right"
  BlackE        -> Debug.crash "right"

rbheight t = case t of
  RBE         -> 0
  T _ l _ r -> 1 + Basics.max (rbheight l) (rbheight r)
  BlackE        -> 0

rbsize t = case t of
  RBE         -> 0
  T _ l _ r -> 1 + rbsize l + rbsize r
  BlackE        -> 0
rbbso t =
  let nonDecreasing xs =
    case xs of
      x1::x2::rest -> x1 <= x2 && nonDecreasing (x2::rest)
      _            -> True
  in
  nonDecreasing (rbtoList t)

rbtoList : RBTree a -> List a
rbtoList t = case t of
  RBE                -> []
  T _ left x right -> rbtoList left ++ [x] ++ rbtoList right
  BlackE               -> []

rbblackHeight t = case t of
  RBE -> Just 0
  T c l _ r ->
    rbblackHeight l |> Maybe.andThen (\n ->
    rbblackHeight r |> Maybe.andThen (\m ->
      if n /= m then Nothing
      else if c == Black then Just (1 + n)
      else Just n
    ))
  BlackE -> Just 1

rbokBlackHeight t = case rbblackHeight t of
  Just _  -> True
  Nothing -> False

rbbh t =
  case rbblackHeight t of
    Just n  -> n
    Nothing -> Debug.crash "bh"

rbnoRedRed t = case t of
  RBE                   -> True
  T Red (T Red _ _ _) _ _ -> False
  T Red _ _ (T Red _ _ _) -> False
  T _ l _ r           -> rbnoRedRed l && rbnoRedRed r
  BlackE                  -> True

rboneRedRed t = case t of
  RBE                             -> False
  T Red (T Red _ _ _) _ (T Red _ _ _) -> False
  T Red (T Red l1 _ r1) _ r         -> rbnoRedRed l1 && rbnoRedRed r1 && rbnoRedRed r
  T Red l _ (T Red l2 _ r2)         -> rbnoRedRed l && rbnoRedRed l2 && rbnoRedRed r2
  T _ l _ r                     -> False
  BlackE                            -> False

rbmaybeOneRedRed t = rboneRedRed t || rbnoRedRed t

rbrb t = rbbso t && rbnoRedRed t && rbokBlackHeight t

------------------------------------------------------------------------------
rbmapToTree : RBTree Int -> TreeDiagram.Tree (Maybe ( Int, Color ))
rbmapToTree t =
  case t of
    RBE -> TreeDiagram.node Nothing []
    BlackE -> TreeDiagram.node Nothing []
    T c l v r -> TreeDiagram.node (Just (v, c)) [(rbmapToTree l), (rbmapToTree r) ]

(=>) prop value =
    prop (toString value)


{-| Represent edges as arrows from parent to child
-}
drawEdge : ( Float, Float ) -> Svg msg
drawEdge ( x, y ) =
    let
        arrowOffset =
            42

        theta =
            atan (y / x)

        rot_ =
            if x > 0 then
                theta
            else
                pi + theta

        rot =
            (rot_ / (2 * pi)) * 360

        dist =
            sqrt (x ^ 2 + y ^ 2)

        scale =
            (dist - arrowOffset) / dist

        ( xTo, yTo ) =
            ( scale * x, scale * y )
    in
        g
            []
            [ line
                [ x1 => 0, y1 => 0, x2 => xTo, y2 => yTo, stroke "black", strokeWidth "2" ]
                []
            , g
                [ transform <|
                    "translate("
                        ++ (toString xTo)
                        ++ " "
                        ++ (toString yTo)
                        ++ ") "
                        ++ "rotate("
                        ++ (toString rot)
                        ++ ")"
                ]
                [ arrow ]
            ]


{-| Represent nodes as colored circles with the node value inside.
-}
drawNode : Maybe ( Int, Color ) -> Svg msg
drawNode n =
    case n of
        Just ( n, c ) ->
            let
                color =
                    case c of
                        Red ->
                            "#CC0000"
                        RedRed ->
                            "#CC0000"

                        Black ->
                            "black"
                        BlackBlack ->
                            "black"
            in
                g
                    []
                    [ circle [ r "27", stroke "black", strokeWidth "2", fill color, cx "0", cy "0" ] []
                    , Svg.text_ [ textAnchor "middle", fill "white", fontSize "30", fontFamily "\"Times New Roman\",serif", transform "translate(0,11)" ] [ Svg.text <| toString n ]
                    ]

        Nothing ->
            g
                []
                [ rect [ Svg.Attributes.width "50", Svg.Attributes.height "35", stroke "black", transform "translate(-25,-22)" ] []
                , Svg.text_ [ textAnchor "middle", fill "white", fontSize "20", transform "translate(0,4)", fontFamily "sans-serif" ] [ Svg.text "Nil" ]
                ]



-- Arrow to go on the ends of edges


arrow =
    polygon [ points "-10,10 10,0, -10,-10 -5,0" ] []
