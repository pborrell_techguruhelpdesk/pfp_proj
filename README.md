At the moment this will just insert and build a red black tree


The draw function in the program from brenden/elm-tree-diagram v3 doesn't seem to update on delete.
Had to add a function to convert a tree like the one used for the redblack tree to one that would work with the graphical output profile.

TODO 
#1 add delete functions (may require more modifications than I'd perfer)
#2 add more models, I would like the heap and we can send unique colours to it for the heap it self and it could build next to the redblack tree.
#3 Make this more than what I would conider a late beta